# dotfiles

Based off [twpayne/dotfiles](https://github.com/twpayne/dotfiles)

Managed with [`chezmoi`](https://www.chezmoi.io/)

## Initialization:

1. Install

    `sh -c "$(curl -fsLS get.chezmoi.io)" -- -b $HOME/.local/bin`
2. Apply

    `$HOME/.local/bin/chezmoi init --apply --ssh gitlab.com/wombat-drone/dotfiles.git`
